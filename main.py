
class MachineRunException(Exception):
    pass

class Machine:
    def __init__(self):
        self.noValues = 30
        self.values = [0]*self.noValues
        self.valPtr = 0
        self.cmdPtr = 0
        self.cmds = []
        self.cellSize = 2**8

    def print(self):
        currentValPtr = ['   '] * self.noValues
        currentValPtr[self.valPtr] = 'vvv'
        print(' '.join(currentValPtr))
        print(' '.join(str(v).zfill(3) for v in self.values))
        currentCmdPtr = [' ']*(self.cmdPtr-1)
        currentCmdPtr.append('v')
        print(''.join(currentCmdPtr))
        print(''.join(c.symbol for c in self.cmds))
        print('_'*100)

    def execute(self, tokens):
        self.cmds = tokens
        while (self.cmdPtr < len(self.cmds)):
            try:
                #self.print()
                tokens[self.cmdPtr].execute(self)
            except MachineRunException as ex:
                print('error at cmd index: {0} | {1}'.format(self.cmdPtr, ex))
                raise ex
            self.cmdPtr += 1

class Token:
    def __str__(self):
        return 'Token: {0}'.format(self.symbol)
    def __repr__(self):
        return self.symbol

class Greater(Token):
    symbol = '>'
    def execute(self, machine):
        machine.valPtr += 1
        if (machine.valPtr) > machine.noValues: raise MachineRunException('memory exceeded')

class Less(Token):
    symbol = '<'
    def execute(self, machine):
        machine.valPtr -= 1
        if (machine.valPtr) < 0: raise MachineRunException('negative memory access')

class Plus(Token):
    symbol = '+'
    def execute(self, machine):
        machine.values[machine.valPtr] += 1
        machine.values[machine.valPtr] %= machine.cellSize

class Minus(Token):
    symbol = '-'
    def execute(self, machine):
        machine.values[machine.valPtr] -= 1
        machine.values[machine.valPtr] %= machine.cellSize

class Dot(Token):
    symbol = '.'
    def execute(self, machine):
        print(chr(machine.values[machine.valPtr]), end='')

class Comma(Token):
    symbol = ','
    def execute(self, machine):
        machine.values[machine.valPtr] = ord(input()) % machine.cellSize

class OpenBracket(Token):
    symbol = '['
    def execute(self, machine):
        if (machine.values[machine.valPtr] == 0):
            startIdx = machine.cmdPtr
            nestLevels = 1
            while(nestLevels != 0 and machine.cmdPtr < len(machine.cmds)):
                if(type(machine.cmds[machine.cmdPtr+1]) == OpenBracket): nestLevels += 1
                if(type(machine.cmds[machine.cmdPtr+1]) == CloseBracket): nestLevels -= 1
                machine.cmdPtr -= 1
            if(machine.cmdPtr == -1):
                machine.cmdPtr = startIdx
                raise MachineRunException('no matching close bracket found')

class CloseBracket(Token):
    symbol = ']'
    def execute(self, machine):
        if (machine.values[machine.valPtr] != 0):
            startIdx = machine.cmdPtr
            nestLevels = 1
            while(nestLevels != 0 and machine.cmdPtr >= 0):
                if(type(machine.cmds[machine.cmdPtr-1]) == OpenBracket): nestLevels -= 1
                if(type(machine.cmds[machine.cmdPtr-1]) == CloseBracket): nestLevels += 1
                machine.cmdPtr -= 1
            if(machine.cmdPtr == -1):
                machine.cmdPtr = startIdx
                raise MachineRunException('no matching open bracket found')

def tokenize(input_str):
    tokenMap = {tkn.symbol: tkn for tkn in [Greater, Less, Plus, Minus, Dot, Comma, OpenBracket, CloseBracket]}
    tokens = []
    for idx, c in enumerate(input_str):
        if c not in tokenMap: raise Exception('Syntax error at {0}'.format(idx))
        tokens.append(tokenMap[c]())
    return tokens

code_input = '>+++++++[<++++>-]<[>+>++++<<-]>>[>+>+>+[<]>>-]>-.----.[<]<++++.>>>++.<------.>>++++.<<----.>.<+++.'
tokens = tokenize(code_input)
machine = Machine()
machine.execute(tokens)

# debug
print()
machine.print()